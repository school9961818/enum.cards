using System;

class Program
{

    // Enum je speciální datový typ, který může nabývat jen předem nadefinovaných hodnot.

    public enum BarvyKaret
    {
        Srdce,
        Kary,
        Piky,
        Krize
    }

    public static void Main(string[] args)
    {
        // Takto potom vypadá proměnná
        BarvyKaret karta = BarvyKaret.Srdce;

        // Můžeme ji vypsat na obrazovku
        Console.WriteLine(karta);

        // Spusťte program. Poté prostudujte: https://www.tutorialsteacher.com/csharp/csharp-enum

        // Teď už víme, že enum je vlastně převlečený int.
        // Můžeme si to představit jako array celých čísel. Začíná na 0 (Srdce) a končí na 3 (Krize)
        // Odkomentujte následující řádek a ověřte
        //Console.WriteLine((int)karta);

        // Převod funguje na obě strany. 
        //Console.WriteLine((BarvyKaret)2);

        // Zkuste co se stane pokud minete rozsah. Upravte předchozí řádek na číslo 5 a zkuste spustit.

        // 1. Napište kód, který vygeneruje náhodnou barvu karty a vypíše na obrazovku.

        // 2. Napište kód, který vygeneruje náhodnou kartu (rozsah 2-10) a vypíše na obrazovku. Např. "7 Srdce"

        // 3. Napište kód, který vygeneruje náhodnou ruku (5 karet) a vypíše je seřazené na obrazovku.
        // Např: "2 Kary, 6 Piky, 6 Srdce, 8 Krize, 9 Krize"

        // Bonus: Napište kód, který vygeneruje dvě náhodné ruce a porovná, která je větší.
        // (Pokerová pravidla jen s dvojicemi a trojicemi. Neřešte full-house, flash a postupky všeho druhu)   
    }
}
